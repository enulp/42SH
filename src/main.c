#include "input/read_input.h"
#include "shell/shell.h"


int main(int argc, char **argv)
{
    struct shell *shell = init_shell();
    int i;
    for (i = 1; i < argc; i++)
    {
        if (argv[i][0] == '-')
            shell_options_parse(shell, argv[i]);
        if (shell->input == 1 && i + 1 <argc)
            shell->file = argv[i + 1];
    }
    int ret;
    if (shell->input)
        ret = read_file(shell, shell->file);
    else
        ret = read_input(shell);
    free(shell);
    return ret;
}
