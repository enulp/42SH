#include "conditions.h"

struct tree *tree_token_compound_list_break(struct list **token_list)
{
    struct tree *tree = init_tree();
    tree->token = TOKEN_COMPOUND_LIST_BREAK;
    tree->node = calloc(1, sizeof(struct node_token_compound_list_break));
    struct node_token_compound_list_break *node = tree->node;
    node->and_or = tree_token_and_or(token_list);
    if (!node->and_or)
    {
        free(tree->node);
        free(tree);
        return NULL;
    }
    struct list_token *tmp = token_peek(*token_list);
    if (tmp->token == TOKEN_AMPERSAND  || tmp->token == TOKEN_SEMICOLON || tmp->token == TOKEN_NEWLINE)
    {
        tmp = token_pop(*token_list);
        free(tmp);
        struct tree *tmp_tree = tree_token_compound_list_break(token_list);
        node->next = tmp_tree;
    }
    else
    {
        free(tree->node);
        free(tree);
    }
    return tree;
}
