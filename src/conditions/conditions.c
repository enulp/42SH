#include "conditions.h"

void print_tree(struct tree *tree)
{
    printf("(");
    if (tree->left)
        print_tree(tree->left);
    printf("%d", tree->token);
    if (tree->right)
        print_tree(tree->right);
    printf(")");
}
/*
   int is_token_semicolon(struct list *token_list)
   {
   if (token_list->head->token == TOKEN_WORD && token_list->head)
   {
   if (token_list->head->token == TOKEN_SEMICOLON && token_list->head)
   {
   if (token_list->head->token == TOKEN_WORD)
   return 1;
   }
   }
   return 0;
   }
   struct tree *tree_token_semicolon(struct list *token_list)
   {
   struct tree *tree = init_tree();
   if (is_token_semicolon(token_list) == 1)
   {
   tree->left = init_tree();
   tree->right = init_tree();
   tree->left->list_token = token_pop(token_list);
   tree->list_token = token_pop(token_list);
   tree->right->list_token = token_pop(token_list);
   }
   return tree;
   }*/
