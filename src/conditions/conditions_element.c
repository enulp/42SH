#include "conditions.h"

struct tree *tree_token_element(struct list **token_list)
{
    struct tree *tree = init_tree();
    tree->token = TOKEN_ELEMENT;
    tree->node = malloc(sizeof(struct node_token_element));
    struct node_token_element *node = tree->node;
    struct list_token *tmp = token_peek(*token_list);
    struct tree *tmp_tree;
    if ((tmp_tree = tree_token_redirection(token_list)))
    {
        node->which = 2;
        node->redirection = tmp_tree;
    }
    else if (tmp->token == TOKEN_WORD)
    {
        tmp = token_pop(*token_list);
        node->which = 1;
        node->word = malloc(256);
        strcpy(node->word, tmp->data);
        free(tmp->data);
        free(tmp);
    }
    else if ((tmp_tree = tree_token_echo(token_list)))
    {
        node->which = 3;
        node->echo = tmp_tree;
    }
    else if ((tmp_tree = tree_token_continue(token_list)))
    {
        node->which = 4;
        node->continu = tmp_tree;
    }
    else if ((tmp_tree = tree_token_break(token_list)))
    {
        node->which = 5;
        node->brea = tmp_tree;
    }
    else
    {
        free(tree->node);
        free(tree);
        return NULL;
    }
    return tree;
}
