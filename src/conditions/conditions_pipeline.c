#include "conditions.h"

struct tree *tree_token_pipeline(struct list **token_list)
{
    struct tree *tree = init_tree();
    tree->token = TOKEN_PIPELINE;
    tree->node = calloc(1, sizeof(struct node_token_pipeline));
    struct node_token_pipeline *node = tree->node;
    node->command_one = tree_token_command(token_list);
    if (!node->command_one)
    {
        free(tree->node);
        free(tree);
        return NULL;
    }
    struct list_token *tmp = token_peek(*token_list);
    if (tmp && tmp->token == TOKEN_PIPE)
        node->command_two = tree_token_command(token_list);
    return tree;
}
