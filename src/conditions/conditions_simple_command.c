#include "conditions.h"

struct tree *tree_token_simple_command(struct list **token_list)
{
    struct tree *tree = init_tree();
    tree->token = TOKEN_SIMPLE_COMMAND;
    tree->node = calloc(1, sizeof(struct node_token_simple_command));
    struct node_token_simple_command *node = tree->node;
    node->which = 2;
    node->element = tree_token_element(token_list);
    //node->element_bis = tree_token_element(token_list);
    if (node->element)
        return tree;
    free(tree->node);
    free(tree);
    return NULL;
}
