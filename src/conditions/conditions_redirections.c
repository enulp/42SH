#include "conditions.h"

struct tree *tree_token_redirection(struct list **token_list)
{
    struct tree *tree = init_tree();
    tree->token = TOKEN_REDIRECTION;
    tree->node = calloc(1, sizeof(struct node_token_redirection));
    struct node_token_redirection *node = tree->node;
    struct list_token *tmp = token_peek(*token_list);
    if (tmp->token == TOKEN_WORD)
    {
        node->ionumber = (tmp)->data;
        tmp = tmp->next;
        if (!tmp)
        {
            free(tree->node);
            free(tree);
            return NULL;
        }
        if (tmp->token == TOKEN_REDIRECTION_ONE)
        {
            tmp = token_pop(*token_list);
            free(tmp);
            tmp = token_pop(*token_list);
            node->which = 1;
            node->word = (token_pop(*token_list))->data;
        }
        else if (tmp->token == TOKEN_REDIRECTION_TWO)
        {
            tmp = token_pop(*token_list);
            free(tmp);
            tmp = token_pop(*token_list);
            node->which = 2;
            node->word = (token_pop(*token_list))->data;
        }
        else if (tmp->token == TOKEN_REDIRECTION_THREE)
        {
            tmp = token_pop(*token_list);
            free(tmp);
            free(tmp);
            node->which = 3;
            node->word = (token_pop(*token_list))->data;
        }
        else if (tmp->token == TOKEN_REDIRECTION_FOUR)
        {
            tmp = token_pop(*token_list);
            free(tmp);
            tmp = token_pop(*token_list);
            node->which = 4;
            node->heredoc = (token_pop(*token_list))->data;
        }
        else if (tmp->token == TOKEN_REDIRECTION_FIVE)
        {
            tmp = token_pop(*token_list);
            free(tmp);
            tmp = token_pop(*token_list);
            node->which = 5;
            node->heredoc = (token_pop(*token_list))->data;
        }
        else if (tmp->token == TOKEN_REDIRECTION_SIX)
        {
            tmp = token_pop(*token_list);
            free(tmp);
            tmp = token_pop(*token_list);
            node->which = 6;
            node->word = (token_pop(*token_list))->data;
        }
        else if (tmp->token == TOKEN_REDIRECTION_SEVEN)
        {
            tmp = token_pop(*token_list);
            free(tmp);
            tmp = token_pop(*token_list);
            node->which = 7;
            node->word = (token_pop(*token_list))->data;
        }
        else if (tmp->token == TOKEN_REDIRECTION_EIGHT)
        {
            tmp = token_pop(*token_list);
            free(tmp);
            tmp = token_pop(*token_list);
            node->which = 8;
            node->word = (token_pop(*token_list))->data;
        }
        else if (tmp->token == TOKEN_REDIRECTION_NINE)
        {
            tmp = token_pop(*token_list);
            free(tmp);
            tmp = token_pop(*token_list);
            node->which = 9;
            node->word = (token_pop(*token_list))->data;
        }
        else if (tmp->token == TOKEN_PIPELINE)
        {
            tmp = token_pop(*token_list);
            free(tmp);
            tmp = token_pop(*token_list);
            node->which = 10;
            node->word = (token_pop(*token_list))->data;
        }
        else
        {
            free(tree->node);
            free(tree);
            return NULL;
        }
        free(tmp);
        return tree;
    }
    else
    {
        free(tree->node);
        free(tree);
        return NULL;
    }
}
