#include "conditions.h"

struct tree *tree_token_command(struct list **token_list)
{
    struct tree *tree = init_tree();
    tree->token = TOKEN_COMMAND;
    tree->node = calloc(1, sizeof(struct node_token_command));
    struct node_token_command *node = tree->node;
    struct tree *tmp;
    if ((tmp = tree_token_simple_command(token_list)))
    {
        node->which = 1;
        node->simple_command = tmp;
    }
    else if ((tmp = tree_token_shell_command(token_list)))
    {
        node->which = 2;
        node->shell_command = tmp;
    }

    else
    {
        free(tree->node);
        free(tree);
        return NULL;
    }
    return tree;
}
