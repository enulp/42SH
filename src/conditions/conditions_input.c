#include "conditions.h"

int tree_token_input(struct list **token_list)
{
    struct list_token *tmp = token_peek(*token_list);
    struct tree *tree = init_tree();
    tree->token = TOKEN_INPUT;
    tree->node = calloc(1, sizeof(struct node_token_input));
    struct node_token_input *node = tree->node;
    node->list = tree_token_list(token_list);
    int ret = eval_token_input(tree);
    free_tree(tree);
    while((tmp = token_pop(*token_list)))
    {
        free(tmp);
    }
    return ret;
}
