#include "conditions.h"
#include "../tree/tree.h"
#include "../token/token.h"
#include "../exec/exec.h"
#include <stdlib.h>
#include <err.h>

struct tree *tree_token_exit(struct list **token_list)
{
    struct tree *tree = init_tree();
    tree->token = TOKEN_EXIT;
    tree->node = calloc(1, sizeof(struct node_token_exit));
    struct node_token_exit *node = tree->node;
    struct list_token *tmp = token_peek(*token_list);
    if (tmp->token == TOKEN_EXIT)
    {
        tmp = token_pop(*token_list);
        free(tmp->data);
        free(tmp);
        tmp = token_pop(*token_list);
        node->value = atoi(tmp->data);
        free(tmp->data);
        free(tmp);
    }
}
