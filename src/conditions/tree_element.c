#include "conditions.h"
#include <stdlib.h>

struct tree *tree_token_element(struct list **token_list)
{
    struct tree *tree = init_tree();
    tree->token = TOKEN_ELEMENT;
    tree->node = malloc(sizeof(struct node_token_element));
    struct node_token_element *node = tree->node;
    struct list_token *tmp = token_peek(*token_list);
    struct tree *tmp_tree;
    if ((tmp_tree = tree_token_redirection(token_list)))
    {
        node->which = 2;
        node->redirection = tmp_tree;
    }
    else if (tmp->token == TOKEN_WORD)
    {
        node->which = 1;
        node->word = tmp->data;
        tmp = token_pop(*token_list);
        free(tmp);
    }
    else if (tmp_tree == tree_token_exit(token_list))
    {
        node->which = 3;
        node->exit = tmp_tree;
        tmp = token_pop(*token_list);
        free(tmp);
        return tree;
    }
    else
    {
        free(tree->node);
        free(tree);
        return NULL;
    }
    return tree;
}
