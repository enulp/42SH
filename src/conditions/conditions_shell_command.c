#include "conditions.h"

struct tree *tree_token_shell_command(struct list **token_list)
{
    struct tree *tree = init_tree();
    tree->token = TOKEN_SHELL_COMMAND;
    tree->node = calloc(1, sizeof(struct node_token_shell_command));
    struct node_token_shell_command *node = tree->node;
    struct list_token *tmp = token_peek(*token_list);
    if (tmp->token == TOKEN_PARENTHESE_OPEN)
    {
        node->which = 1;
        node->compound_list_break = tree_token_compound_list_break(token_list);
        tmp = token_pop(*token_list);
        if (tmp->token == TOKEN_PARENTHESE_CLOSE)
        {
            free(tmp);
            return tree;
        }
        errx(1, "missing TOKEN_PARENTHSE_CLOSE");
    }
    /*else if (tmp->token == TOKEN_WAVEBRACKET_OPEN)
      {
      node->which = 2;
      node->compound_list = tree_token_compound_list(token_list);
      tmp->token == TOKEN_WAVEBRACKET_CLOSE;
      if (tmp->token == TOKEN_WAVEBRACKET_CLOSE)
      return tree;
      errx(1, "missing TOKEN_WAVEBRACKET_CLOSE");
      }*/
      else
      {
          struct tree *tmp;
          /*if ((tmp = tree_token_rule_for(token_list)))
            {
            node->which = 3;
            node->rule_for = tree_token_rule_for(token_list);
            }*/
          if ((tmp = tree_token_rule_while(token_list)))
          {
              node->which = 4;
              node->rule_while = tmp;
              return tree;
          }
             if ((tmp = tree_token_rule_until(token_list)))
             {
             node->which = 5;
             node->rule_until = tmp;
              return tree;
             }
             /*if ((tmp = tree_token_rule_case(token_list)))
             {
             node->which = 6;
             node->rule_case = tmp;
             }*/
          if ((tmp = tree_token_rule_if(token_list)))
          {
              node->which = 7;
              node->rule_if = tmp;
              return tree;
          }
      }
      free(tree->node);
      free(tree);
      return NULL;
}
