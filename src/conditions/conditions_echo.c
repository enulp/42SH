#include "conditions.h"

struct tree *tree_token_echo(struct list **token_list)
{
    struct list_token *tmp = token_peek(*token_list);
    if (tmp->token != TOKEN_ECHO)
        return NULL;
    tmp = token_pop(*token_list);
    free(tmp);
    struct tree *tree = init_tree();
    tree->node = calloc(1, sizeof(struct node_token_echo));
    struct node_token_echo *node = tree->node;
    tree->token = TOKEN_ECHO;
    tmp = token_peek(*token_list);
    if (tmp->token != TOKEN_WORD)
       return tree;
    tmp = token_pop(*token_list);
    node->word = tmp->data;
    free(tmp);
    return tree;
}
