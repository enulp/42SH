#include "conditions.h"

struct tree *tree_token_else_clause(struct list **token_list)
{
    struct tree *tree = init_tree();
    tree->token = TOKEN_ELSE_CLAUSE;
    tree->node = calloc(1, sizeof(struct node_token_else_clause));
    struct node_token_else_clause *node = tree->node;
    struct list_token *tmp = token_pop(*token_list);
    if (tmp->token == TOKEN_ELSE)
    {
        node->which = 1;
        node->compound_list_break_one = tree_token_compound_list_break(token_list);
    }
    else if (tmp->token == TOKEN_ELIF)
    {
        node->which = 2;
        node->compound_list_break_one = tree_token_compound_list_break(token_list);
        tmp = token_peek(*token_list);
        if (tmp && tmp->token == TOKEN_THEN)
            tmp = token_pop(*token_list);
        else
            errx(1, "missing TOKEN_THEN");
        node->compound_list_break_two = tree_token_compound_list_break(token_list);
        tmp = token_peek(*token_list);
        if (tmp && (tmp->token == TOKEN_ELIF || tmp->token == TOKEN_ELSE))
            node->else_clause = tree_token_else_clause(token_list);
    }
    else
        errx(1, "missing TOKEN_ELSE");
    return tree;
}
