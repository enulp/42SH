#include "conditions.h"

struct tree *tree_token_do_group(struct list **token_list)
{
    struct list_token *tmp = token_pop(*token_list);
    if (tmp->token != TOKEN_DO)
        errx(1, "missing TOKEN_DO");
    struct tree *tree = init_tree();
    tree->token = TOKEN_DO_GROUP;
    tree->node = calloc(1, sizeof(struct node_token_do_group));
    struct node_token_do_group *node = tree->node;
    node->compound_list_break = tree_token_compound_list_break(token_list);
    free(tmp);
    tmp = token_pop(*token_list);
    if (tmp && tmp->token != TOKEN_DONE)
        errx(1, "missing TOKEN_DONE");
    free(tmp);
    return tree;
}
