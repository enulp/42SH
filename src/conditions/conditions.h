#include "../tree/tree.h"
#include "../token/token.h"
#include "../exec/exec.h"
#include <stdlib.h>
#include <err.h>
#include <stdio.h>

#ifndef CONDITIONS_H
#define CONDITIONS_H

struct tree *tree_token_echo(struct list **token_list);
/*! \brief Checks if the first element of a token list is a TOKEN_WORD
 *
 *  This function takes a token list as parameter and returns 1 if the first
 *  element of the list is a TOKEN_WORD, 0 otherwise
 *
 * \param struct list *token_list: a list of tokens whose head is to be checked
 * \return int: returns 1 if the first element of the list is a TOKEN_WORD,
 *      0 otherwise
 */
int is_token_element(struct list *token_list);

/*! \brief Builds a tree corresponding to TOKEN_WORD
 *
 *  This function takes a token list as parameter and builds a tree
 *
 * \param struct list *token_list: a list of tokens whose head is to be checked
 * \return struct tree: a tree corresponding to a TOKEN_WORD
 */
struct tree *tree_token_element(struct list **token_list);

/*! \brief Checks if the first element of a token list is a TOKEN_WORD
 *
 *  This function takes a token list as parameter and returns 1 if the first
 *  element of the list is a TOKEN_SEMICOLON, 0 otherwise
 *
 * \param struct list *token_list: a list of tokens whose head is to be checked
 * \return int: returns 1 if the first element of the list is a TOKEN_SEMICOLON,
 *      0 otherwise
 */
int is_token_semicolon(struct list *token_list);

/*! \brief Builds a tree corresponding to TOKEN_SEMICOLON
 *
 *  This function takes a token list as parameter and builds a tree
 *
 * \param struct list *token_list: a list of tokens whose head is to be checked
 * \return struct tree: a tree corresponding to a TOKEN_SEMICOLON
 */
struct tree *tree_token_semicolon(struct list *token_list);
struct tree *tree_token_exit(struct list **token_list);
struct tree *tree_token_rule_if(struct list **token_list);
struct tree *tree_token_else_clause(struct list **token_list);
struct tree *tree_token_redirection(struct list **token_list);
struct tree *tree_token_simple_command(struct list **token_list);
struct tree *tree_token_do_group(struct list **token_list);
struct tree *tree_token_rule_while(struct list **token_list);
struct tree *tree_token_list(struct list **token_list);
int tree_token_input(struct list **token_list);
struct tree *tree_token_rule_until(struct list **token_list);
struct tree *tree_token_shell_command(struct list **token_list);
struct tree *tree_token_command(struct list **token_list);
struct tree *tree_token_pipeline(struct list **token_list);
struct tree *tree_token_and_or(struct list **token_list);
struct tree *tree_token_compound_list_break(struct list **token_list);
struct tree *tree_token_continue(struct list **token_list);
struct tree *tree_token_break(struct list **token_list);
#endif
