#include "conditions.h"


struct tree *tree_token_list(struct list **token_list)
{
    struct list_token *tmp = token_peek(*token_list);
    tmp = tmp;
    struct tree *tree = init_tree();
    tree->token = TOKEN_LIST;
    tree->node = calloc(1, sizeof(struct node_token_list));
    struct node_token_list *node = tree->node;
    node->and_or = tree_token_and_or(token_list);
    tmp = token_peek(*token_list);
    if (tmp && tmp->token == TOKEN_SEMICOLON)
    {
        tmp = token_pop(*token_list);
        free(tmp);
        node->next = tree_token_list(token_list);
    }
    return tree;
}
