#include "conditions.h"

struct tree *tree_token_rule_while(struct list **token_list)
{
    struct list_token *tmp = token_peek(*token_list);
    if (tmp->token != TOKEN_WHILE)
        return NULL;
    tmp = token_pop(*token_list);
    free(tmp);
    struct tree *tree = init_tree();
    tree->token = TOKEN_RULE_WHILE;
    tree->node = calloc(1, sizeof(struct node_token_rule_while));
    struct node_token_rule_while *node = tree->node;
    node->compound_list_break = tree_token_compound_list_break(token_list);
    node->do_group = tree_token_do_group(token_list);
    return tree;
}
