#include "conditions.h"

struct tree *tree_token_break(struct list **token_list)
{
    struct list_token *tmp = token_peek(*token_list);
    if (tmp->token != TOKEN_BREAK)
        return NULL;
    tmp = token_pop(*token_list);
    free(tmp);
    struct tree *tree = init_tree();
    tree->node = calloc(1, sizeof(struct node_token_break));
    tree->token = TOKEN_BREAK;
    return tree;
}
