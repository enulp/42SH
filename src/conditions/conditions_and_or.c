#include "conditions.h"

struct tree *tree_token_and_or(struct list **token_list)
{
    struct list_token *tmp = token_peek(*token_list);
    struct tree *tree = init_tree();
    tree->token = TOKEN_AND_OR;
    tree->node = calloc(1, sizeof(struct node_token_and_or));
    struct node_token_and_or *node = tree->node;
    node->pipeline_one = tree_token_pipeline(token_list);
    if (!node->pipeline_one)
    {
        free(tree->node);
        free(tree);
        return NULL;
    }
    tmp = token_peek(*token_list);
    if (!tmp)
        return tree;
    if (tmp->token == TOKEN_AND)
        node->and_or = 1;
    else if (tmp->token == TOKEN_OR)
        node->and_or = 2;
    else
        return tree;
    tmp = token_pop(*token_list);
    free(tmp);
    node->pipeline_two = tree_token_pipeline(token_list);
    return tree;
}
