#include "conditions.h"

struct tree *tree_token_rule_if(struct list **token_list)
{
    struct list_token *tmp = token_peek(*token_list);
    struct tree *tree = init_tree();
    tree->token = TOKEN_RULE_IF;
    tree->node = calloc(1, sizeof(struct node_token_rule_if));
    struct node_token_rule_if *node = tree->node;
    if (tmp->token == TOKEN_IF)
        tmp = token_pop(*token_list);
    else
    {
        free(tree->node);
        free(tree);
        return NULL;
    }
    free(tmp);
    node->compound_list_break_one  = tree_token_compound_list_break(token_list);
    tmp = token_pop(*token_list);
    if (tmp && tmp->token == TOKEN_THEN)
    {
        free(tmp);
        node->compound_list_break_two = tree_token_compound_list_break(token_list);
    }
    else
        errx(1, "missing TOKEN_THEN");
    tmp = token_peek(*token_list);
    if (tmp && (tmp->token == TOKEN_ELSE || tmp->token == TOKEN_ELIF))
    {
        node->else_clause = tree_token_else_clause(token_list);
        tmp = token_peek(*token_list);
    }
    tmp = token_pop(*token_list);
    if (tmp && tmp->token == TOKEN_FI)
    {
        free(tmp);
        return tree;
    }
    else
        errx(1, "missing TOKEN_FI");
    return tree;
}
