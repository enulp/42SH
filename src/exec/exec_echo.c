#include "exec.h"

int eval_token_echo(struct tree *tree)
{
    struct node_token_echo *node = tree->node;
    char *word = node->word;
    for (size_t i = 0; i < strlen(word); i++)
    {
        char l = word[i];
        if (l == '\\' && i < strlen(word) - 1)
        {
            l = word[i + 1];
            if (l == '\\')
                printf("\\");
            if (l == 'a')
                printf("\a");
            if (l == 'b')
                printf("\b");
            if (l == 'c')
                break;
            if (l == 'f')
                printf("\f");
            if (l == 'r')
                printf("\r");
            if (l == 'n')
                printf("\n");
            if (l == 'v')
                printf("\v");
            if (l == 't')
                printf("\t");
        }
        else
            printf("%c", l);
    }
    printf("\n");
    return 0;
}
