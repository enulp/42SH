#include <assert.h>
#include <stdio.h>
#include <readline/history.h>
#include <readline/readline.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <unistd.h>
#include "err.h"
#include <sys/stat.h>
#include <fcntl.h>
#include "exec.h"
#include "../token/token.h"

int eval_token_redirection(struct tree *tree)
{
    struct node_token_redirection *node = tree->node;
    char *ionumber = node->ionumber;
    if (!ionumber)
        ionumber = "";
    int which = node->which;
    char *word = node->word;
    //char *heredoc = node->heredoc;
    int fd = 0;
    if (!fork())
    {
        switch(which)
        {
        case 8:
        case 1:
            close(1);
            fd = open(word, O_WRONLY | O_CREAT | O_TRUNC, 0666);
            dup(fd);
            exec(ionumber);
            break;
        case 2:
            close(1);
            fd = open(word, O_RDONLY);
            dup2(fd, 0);
            exec(ionumber);
            break;
        case 3:
            close(1);
            fd = open(word, O_RDWR | O_APPEND, 0666);
            dup(fd);
            exec(ionumber);
            break;
        case 9:
            close(1);
            fd = open(word, O_RDWR | O_CREAT, 0666);
            dup(fd);
            exec(ionumber);
            break;
        case 10:
            which = which;
            int pipefd[2];
            int pid;
            //char words[20];
            char *f1_args = strtok(node->ionumber, " ");
            char *f2_args = strtok(node->word, " ");
            pipe(pipefd);
            pid = fork();
            if (!pid)
            {
                dup2(pipefd[0], 0);
                close(pipefd[1]);
                execvp(&f2_args[0], &f2_args);
            }
            else
            {
                waitpid(pid, NULL, 0);
                dup2(pipefd[1], 1);
                close(pipefd[0]);
                execvp(&f1_args[0], &f1_args);
            }
            break;
        default:
            break;
        }
    }
    else
    {
        close(fd);
        wait(NULL);
    }
    return 1;
}
