#include "exec.h"

int eval_token_compound_list_break(struct tree *tree)
{
    struct node_token_compound_list_break *node = tree->node;
    struct tree *and_or = node->and_or;
    struct tree *next = node->next;
    int ret = eval_token_and_or(and_or);
    if (ret == -100)
        return 0;
    if (ret == -101)
        return -101;
    if (next)
        ret = eval_token_compound_list_break(next);
    return ret;
}
