#include "exec.h"

int eval_token_rule_if(struct tree *tree)
{
    struct node_token_rule_if *node = tree->node;
    struct tree *condition = node->compound_list_break_one;
    struct tree *then = node->compound_list_break_two;
    struct tree *else_clause = node->else_clause;
    if (!eval_token_compound_list_break(condition))
        return eval_token_compound_list_break(then);
    else if (else_clause)
        return eval_token_else_clause(else_clause);
    return 0;
}
