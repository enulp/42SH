#include "exec.h"

int eval_token_rule_until(struct tree *tree)
{
    struct node_token_rule_until *node = tree->node;
    struct tree *compound_list_break = node->compound_list_break;
    struct tree *do_group = node->do_group;
    int ret = 0;
    while (eval_token_compound_list_break(compound_list_break))
        ret = eval_token_do_group(do_group);
    return ret;
}
