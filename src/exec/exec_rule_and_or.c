#include "exec.h"

int eval_token_and_or(struct tree *tree)
{
    struct node_token_and_or *node = tree->node;
    struct tree *pipeline_one = node->pipeline_one;
    struct tree *pipeline_two = node->pipeline_two;
    int return_value_one = eval_token_pipeline(pipeline_one);
    if (!node->and_or)
        return return_value_one;
    int return_value_two = eval_token_pipeline(pipeline_two);
    if (node->and_or == 1)
        return return_value_one && return_value_two;
    else
        return return_value_one || return_value_two;
}
