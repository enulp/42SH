#include "exec.h"

int eval_token_list(struct tree *tree)
{
    struct node_token_list *node = tree->node;
    struct tree *and_or = node->and_or;
    struct tree *next = node->next;
    int ret = eval_token_and_or(and_or);
    if (next)
        ret = eval_token_list(next);
    return ret;
}
