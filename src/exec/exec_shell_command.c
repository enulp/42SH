#include "exec.h"

int eval_token_shell_command(struct tree *tree)
{
    struct node_token_shell_command *node = tree->node;
    struct tree *compound_list_break = node->compound_list_break;
    //struct tree *compound_list = node->compound_list;
    //struct tree *rule_for = node->rule_for;
    struct tree *rule_while = node->rule_while;
    struct tree *rule_until = node->rule_until;
    //truct tree *rule_case = node->rule_case;
    struct tree *rule_if = node->rule_if;
    if (node->which == 1)
        return eval_token_compound_list_break(compound_list_break);
    if (node->which == 4)
        return eval_token_rule_while(rule_while);
    if (node->which == 5)
        return eval_token_rule_until(rule_until);
    if (node->which == 7)
        return eval_token_rule_if(rule_if);
    return 0;
}
