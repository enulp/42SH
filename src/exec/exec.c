#include "exec.h"

int eval(struct tree *tree)
{
    return eval_token_input(tree);
}
int exec(char *str)
{
    pid_t child;
    int status;
    char *new;
    asprintf(&new, "%s", str);
    char *token = strtok(new, " ");
    char *words[128];
    int i = 0;
    while (token != NULL)
    {
        if (strlen(token))
        {
            words[i] = malloc(1 + strlen(token));
            strcpy(words[i], token);
        }
        i++;
        token = strtok(NULL, " ");
    }
    words[i] = NULL;
    if (words[0])
    {
        child = fork();
        if (child == 0)
        {
            execvp(words[0], words);
            warn("%s is not found",  words[0]);
        }
        waitpid(child, &status, 0);
    }
    for (i = i - 1; i >= 0; i--)
        free(words[i]);
    free(token);
    free(new);
    return status;
}
