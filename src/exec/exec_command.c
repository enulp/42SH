#include "exec.h"

int eval_token_command(struct tree *tree)
{
    struct node_token_command *node = tree->node;
    struct tree *simple_command = node->simple_command;
    struct tree *shell_command = node->shell_command;
    if (node->which == 1)
        return eval_token_simple_command(simple_command);
    else if (node->which == 2)
        return eval_token_shell_command(shell_command);
    return 0;
}
