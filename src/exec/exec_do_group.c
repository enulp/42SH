#include "exec.h"

int eval_token_do_group(struct tree *tree)
{
    struct node_token_do_group *node = tree->node;
    struct tree *compound_list_break = node->compound_list_break;
    return eval_token_compound_list_break(compound_list_break);
}
