#include "exec.h"

int eval_token_element(struct tree *tree)
{
    struct node_token_element *node = tree->node;
    char *word = node->word;
    struct tree *redirection = node->redirection;
    struct tree *echo = node->echo;
    if (node->which == 1)
        return exec(word);
    else if (node->which == 2)
        return eval_token_redirection(redirection);
    else if (node->which == 3)
        return eval_token_echo(echo);
    else if (node->which == 4)
        return -100;
    else if (node->which == 5)
        return -101;
    return 0;
}
