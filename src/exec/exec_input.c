#include "exec.h"

int eval_token_input(struct tree *tree)
{
    struct node_token_input *node = tree->node;
    struct tree *list = node->list;
    return eval_token_list(list);
}
