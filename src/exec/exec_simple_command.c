#include "exec.h"

int eval_token_simple_command(struct tree *tree)
{
    struct node_token_simple_command *node = tree->node;
    struct tree *element = node->element;
    //struct tree *element_bis = node->element_bis;
    int which = node->which;
    int ret;
    if (which == 2)
    {
        ret = eval_token_element(element);
        //eval_token_element(element_bis);
        return ret;
    }
    return 0;
}
