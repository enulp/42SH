#include "exec.h"

int eval_token_pipeline(struct tree *tree)
{
    struct node_token_pipeline *node = tree->node;
    struct tree *command_one = node->command_one;
    struct tree *command_two = node->command_one;
    if (1)
        return eval_token_command(command_one);

    int fd[2];
    pipe(fd);
    fd[0] = 1;
    pid_t pid;
    pid = fork();

    if (pid == 0)
    {
        fd[1] = 0;
        return eval_token_command(command_two);
    }
    waitpid(-1, NULL, 0);
    return 0;
}
