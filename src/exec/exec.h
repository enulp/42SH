#ifndef EXEC_H
#define EXEC_H

#include <assert.h>
#include <stdio.h>
#include <readline/history.h>
#include <readline/readline.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <unistd.h>
#include "err.h"
#include <sys/stat.h>
#include <fcntl.h>
#include "../token/token.h"
#include "../tree/tree.h"

int eval_token_compound_list_break(struct tree *tree);
int eval_token_rule_if(struct tree *tree);
int eval_token_compound(struct tree *tree);
int eval_token_redirection(struct tree *tree);
int eval_token_rule_until(struct tree *tree);
int eval_token_element(struct tree *tree);
int eval_token_rule_while(struct tree *tree);
int eval_token_simple_command(struct tree *tree);
int eval_token_shell_command(struct tree *tree);
int eval_token_command(struct tree *tree);
int eval_token_pipeline(struct tree *tree);
int eval_token_echo(struct tree *tree);
int eval_token_and_or(struct tree *tree);
int eval_token_do_group(struct tree *tree);
int eval_token_else_clause(struct tree *tree);
int eval_token_list(struct tree *tree);
int eval_token_input(struct tree *tree);

/**
** \brief Simple evaluate function on the tree
*/
int eval(struct tree *tree);

/**
** \brief execute the command at node tree
** \param tree is the tree we want to execute
*/
int exec(char *s);
int eval_token_compound_list_break(struct tree *tree);
int eval_token_exit(struct tree *tree);
int eval_token_input(struct tree *tree);
int eval_token_rule_if(struct tree *tree);
int eval_token_rule_case(struct tree *tree);
int eval_token_compound(struct tree *tree);
int eval_token_do_group(struct tree *tree);
#endif
