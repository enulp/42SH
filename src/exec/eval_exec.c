#include "exec.h"

int eval_token_exit(struct tree *tree)
{
    struct node_token_exit *node = tree->node;
    int value = node->value;
    return value;
}
