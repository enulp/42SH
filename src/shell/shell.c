#include <stdio.h>
#include "../token/token.h"
#include "../tree/tree.h"
#include "shell.h"
#include <stdlib.h>
#include <string.h>

void shell_options_parse(struct shell *shell, char *s)
{
    char buf[256] = { 0 };
    int counter_buf = 0;
    for (; *s != ' ' && *s != '\0'; s++, counter_buf++)
    {
        buf[counter_buf] = *s;
    }
    if (!strcmp(buf, "--ast-print"))
        shell->print_tree = 1;
    if (!strcmp(buf, "-c"))
        shell->input = 1;
}

struct shell *init_shell(void)
{
    struct shell *shell = malloc(sizeof(struct shell));
    shell->print_tree = 0;
    shell->input = 0;
    return shell;
}
