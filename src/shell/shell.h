#ifndef SHELL_H
#define SHELL_H

struct shell
{
    int print_tree;
    //0 is file 1 is string
    int input;
    char *file;
};

/*! \brief Parses options of input string
 *
 *  This function takes a shell and a pointer to a string as arguments, parses
 *  the string to find potential options to set in the shell
 *
 * \param struct shell *shell: the shell which options will be changed
 * \param char **s: a pointer to a string containing the options to be parsed
 */
void shell_options_parse(struct shell *shell, char *s);

/*! \brief Creates a new shell element
 *
 *  This function creates a new shell element and returns it
 *
 * \return struct shell *shell: a new shell element
 */
struct shell *init_shell(void);

#endif
