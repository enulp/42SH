#include <stdio.h>
#include <stdlib.h>
#include "token.h"
#include "../tree/tree.h"
#include "../shell/shell.h"
#include <string.h>
#include "../input/read_input.h"
#include "../token/token.h"

#define ARRAY_SIZE(array) (sizeof(array)/sizeof(*(array)))

struct list_token *list_token_init(void)
{
    struct list_token *list_token = malloc(sizeof(struct list_token));
    list_token->next = NULL;
    list_token->token = EOF;
    return list_token;
}
// 0 not empty 1 empty 2 newline
int not_empty_buffer(char *str)
{
    for (int i = 0; str[i] != '\0'; i++)
    {
        if (!(str[i] == ' '))
            return 0;
        if (!(str[i] == '\n'))
            return 2;
    }

    return 1;
}
int aisb(char *a, char *b)
{
    while (*a == *b)
    {
        if (*b == '\0')
            return 1;
        a++;
        b++;
    }
    if (*b =='\0' && (*a == '\0' || *a == ' ' || *a == '\n' || *a == ';'))
        return 1;
    return 0;
}

int is_special_char(char *s)
{
    if (*s == '\0' || *s == ';' || *s == ')')
        return 1;
    if (*s == '\n')
        return -1;
    char *keywords[] =
    {
        ">", ">>", "exit", "|", "||", "&&", "if", "then", "elif", "else", "fi",
        "do", "done", "while", "until", "for", "in", "case", "esac", "echo",
        "break", "continue"
    };
    size_t arr_size = ARRAY_SIZE(keywords);
    size_t i;
    for (i = 0; i < arr_size; i++)
    {
        if (aisb(s, keywords[i]))
        {
            if (i == 20)
                return 4;
            if (i == 21)
                return 7;
            return strlen(keywords[i]);
        }
    }
    return 0;
}

void tokenize_buf(struct list *token_list, char *buf)
{
    struct list_token *new = list_token_init();
    new->token = TOKEN_WORD;
    new->data = buf;
    if (!token_list->head)
    {
        token_list->head = new;
        token_list->tail = new;
    }
    else
    {
        token_list->tail->next = new;
        token_list->tail = new;
    }
}
void tokenize_special_char(struct list *token_list, char *s)
{
    if (*s == '\0')
        return;
    struct list_token *new = list_token_init();
    if (*s == ';')
    {
        new->data = ";";
        new->token = TOKEN_SEMICOLON;
    }
    else
    {
        char *keywords[] =
        {
            "\n", "<", ">", ">>", "exit", "|", "||", "&&", "if", "then", "elif",
            "else", "fi", "do", "done", "while", "until", "for", "in", "case",
            "esac", "echo", "break", "continue"
        };
        enum token tokens[] =
        {
            TOKEN_NEWLINE, TOKEN_REDIRECTION_TWO, TOKEN_REDIRECTION_ONE,
            TOKEN_REDIRECTION_THREE, TOKEN_EXIT, TOKEN_PIPELINE, TOKEN_OR,
            TOKEN_AND, TOKEN_IF, TOKEN_THEN, TOKEN_ELIF, TOKEN_ELSE, TOKEN_FI,
            TOKEN_DO, TOKEN_DONE, TOKEN_WHILE, TOKEN_UNTIL, TOKEN_FOR, TOKEN_IN,
            TOKEN_CASE, TOKEN_ESAC, TOKEN_ECHO, TOKEN_BREAK, TOKEN_CONTINUE
        };
        int was_found = 0;
        size_t arr_size = ARRAY_SIZE(keywords);
        size_t i;
        for (i = 0; i < arr_size; i++)
        {
            if (aisb(s, keywords[i]))
            {
                new->data = keywords[i];
                new->token = tokens[i];
                was_found = 1;
                break;
            }
        }
        if (!was_found)
        {
            free(new);
            return;
        }
    }
    if (!token_list->head)
        token_list->head = new;
    else
        token_list->tail->next = new;
    token_list->tail = new;
}

int tokenize_string_push(struct list *token_list, char *s)
{
    int buf_counter = 0;
    int string_double = 1;
    int ret = 0;
    for (; *s != '\0'; s++)
    {
        char *buf = calloc(256, sizeof(char));
        while (!is_special_char(s) || string_double < 0)
        {
            if (*s == '"' && string_double < 0)
            {
                string_double *= -1;
                s++;
            }
            else if (*s == '"')
            {
                s++;
                string_double *= -1;
            }
            buf[buf_counter] = *s;
            buf_counter++;
            s++;
        }
        if (buf_counter && !not_empty_buffer(buf))
        {
            buf[buf_counter] = '\0';
            buf_counter = 0;
            tokenize_buf(token_list, buf);
            ret = 1;
        }
        else
            free(buf);
        if (*s == '\0')
            break;
        ret = 1;
        tokenize_special_char(token_list, s);
        if (is_special_char(s) > 0)
            s += is_special_char(s);
    }
    return ret;
}

struct list *tokenize_string(char *s)
{
    struct list *token_list = malloc(sizeof(struct list));
    token_list->head = NULL;
    token_list->tail = NULL;
    int buf_counter = 0;
    int string_double = 1;
    for (; *s != '\0'; s++)
    {
        char *buf = calloc(256, sizeof(char));
        while (!is_special_char(s) || string_double < 0)
        {
            if (*s == '"' && string_double < 0)
            {
                string_double *= -1;
                s++;
            }
            else if (*s == '"')
            {
                s++;
                string_double *= -1;
            }
            buf[buf_counter] = *s;
            buf_counter++;
            s++;
        }
        if (buf_counter && !not_empty_buffer(buf))
        {
            buf[buf_counter] = '\0';
            buf_counter = 0;
            tokenize_buf(token_list, buf);
        }
        else
            free(buf);
        if (*s == '\0' || *s == '\n')
            break;
        tokenize_special_char(token_list, s);
        if (is_special_char(s) > 0)
            s += is_special_char(s);
    }
    return token_list;
}

void print_token(struct list *token_list)
{
    for (struct list_token *list_token = token_list->head; list_token;
            list_token = list_token->next)
    {
        printf("->%s\n", list_token->data);
    }
}

struct list_token *token_peek(struct list *token_list)
{
    return token_list->head;
}

int token_newline(struct list *token_list)
{
    if (!is_interactive())
        return 0;
    char *buffer = malloc(sizeof(char *));
    buffer = get_next_line ("42sh$ ");
    if (buffer == NULL)
        exit(1);
    if (*buffer == '\0')
    {
        free(buffer);
        token_newline(token_list);
    }
    return tokenize_string_push(token_list, buffer);
}

struct list_token *token_pop(struct list *token_list)
{
    struct list_token *tmp = token_list->head;
    if (!tmp)
    {
        if (!is_interactive())
            return NULL;
        if (token_newline(token_list))
            return token_pop(token_list);
        struct list_token *eof = malloc(sizeof(struct list_token));
        eof->token = TOKEN_EOF;
        eof->data = "EOF";
        return eof;
    }
    token_list->head = token_list->head->next;
    return tmp;
}
