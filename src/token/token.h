#include "../shell/shell.h"

#ifndef TOKEN_H
#define TOKEN_H

enum token
{
    TOKEN_EXIT,
    TOKEN_ECHO,
    TOKEN_BREAK,
    TOKEN_CONTINUE,
    TOKEN_REDIRECTION_ONE,
    TOKEN_REDIRECTION_TWO,
    TOKEN_REDIRECTION_THREE,
    TOKEN_REDIRECTION_FOUR,
    TOKEN_REDIRECTION_FIVE,
    TOKEN_REDIRECTION_SIX,
    TOKEN_REDIRECTION_SEVEN,
    TOKEN_REDIRECTION_EIGHT,
    TOKEN_REDIRECTION_NINE,
    TOKEN_IONUMBER,
    TOKEN_SEMICOLON,
    TOKEN_NEWLINE,
    TOKEN_AMPERSAND,
    TOKEN_PIPE,
    TOKEN_AND,
    TOKEN_OR,
    TOKEN_PARENTHESE_OPEN,
    TOKEN_PARENTHESE_CLOSE,
    TOKEN_EXCLAMATION_MARK,
    TOKEN_WAVEBRACKET_OPEN,
    TOKEN_WAVEBRACKET_CLOSE,
    TOKEN_CASE,
    TOKEN_DO,
    TOKEN_DONE,
    TOKEN_ELIF,
    TOKEN_ELSE,
    TOKEN_ESAC,
    TOKEN_FI,
    TOKEN_FOR,
    TOKEN_IF,
    TOKEN_IN,
    TOKEN_THEN,
    TOKEN_UNTIL,
    TOKEN_WHILE,

    TOKEN_INPUT,
    TOKEN_LIST,
    TOKEN_AND_OR,
    TOKEN_PIPELINE,
    TOKEN_COMMAND,
    TOKEN_SIMPLE_COMMAND,
    TOKEN_SHELL_COMMAND,
    TOKEN_FUNDEC,
    TOKEN_REDIRECTION,
    TOKEN_PREFIX,
    TOKEN_ELEMENT,
    TOKEN_WORD,
    TOKEN_COMPOUND_LIST,
    TOKEN_COMPOUND_LIST_BREAK,
    TOKEN_RULE_FOR,
    TOKEN_RULE_WHILE,
    TOKEN_RULE_UNTIL,
    TOKEN_RULE_CASE,
    TOKEN_CASE_CLAUSE,
    TOKEN_CASE_ITEM,
    TOKEN_RULE_IF,
    TOKEN_ELSE_CLAUSE,
    TOKEN_DO_GROUP,

    TOKEN_EOF
};

struct list
{
    struct list_token *head;
    struct list_token *tail;
};

struct list_token
{
    struct list_token *next;
    enum token token;
    char *data;
};

/*! \brief Creates a new element for a token list
 *
 *  This function creates a new element for a token list and returns it
 *
 * \return struct list_token *list_token: a new list_token element
 */
struct list_token *list_token_init(void);

/*! \brief Special command detector
 *
 *  This function takes a char as input and returns 1 if it is a special
 *      character, 0 otherwise
 *
 * \param  char s: a char to be tested.
 * \return int: 1 if s is a special character, 0 otherwise
 */
int is_special_char(char *s);

/*! \brief Tokenizes a string and inserts the created token to a list of tokens
 *
 *  This function takes the parameter buf, creates a new list_token out of it
 *  of type TOKEN_WORD and adds it to the token list token_list.
 *
 * \param  struct list *token_list: pointer to a struct list containing tokens
 * \param  char *buf: a string of characters to be tokenized
 */
void tokenize_buf(struct list *token_list, char *buf);

/*! \brief Tokenizes a special character and inserts the created token
 *      in a list of tokens
 *
 *  This function takes the parameter buf, creates a new list_token out of it
 *  of type TOKEN_WORD and adds it to the token list token_list
 *
 * \param  struct list *token_list: pointer to a struct list containing tokens
 * \param  char *buf: a string of characters to be tokenized
 */
void tokenize_special_char(struct list *token_list, char *s);

/*! \brief Tokenizes a string containing several expressions and inserts them
 *      in a list of tokens
 *
 *  This function takes a string s as parameter, splits it into substrings
 *  and calls tokenize_buf to create new tokens to add to a token list
 *
 * \param  struct list *token_list: pointer to a struct list containing tokens
 * \param  char *buf: a string of characters to be tokenized
 */
struct list *tokenize_string(char *s);

/*! \brief Prints the data contained in a token list
 *
 *  This function takes a token list as parameter, and prints the data
 *  of each token while traversing the list
 *
 * \param struct list *token_list: the list containing the tokens to be printed
 */
void print_token(struct list *token_list);

/*! \brief Returns the first element of a token list
 *
 *  This function takes a token list as parameter and returns the first token
 *  of the list. The token is not removed from the list
 *
 * \param struct list *token_list: a list of tokens which head is to be returned
 * \return struct token_list: the first token of token_list
 */
struct list_token *token_peek(struct list *token_list);

/*! \brief Returns the first element of a token list
 *
 *  This function takes a token list as parameter and returns the first token
 *  of the list. This function behaves similarly to token_peek, but the token
 *  is removed from the list before being returned
 *
 * \param struct list *token_list: a list of tokens which head is to be returned
 * \return struct token_list: the first token of token_list
 */
struct list_token *token_pop(struct list *token_list);
int aisb(char *a, char *b);
#endif
