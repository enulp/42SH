#include "../tree/tree.h"
#include "../conditions/conditions.h"
#include "../token/token.h"
#include <stdlib.h>
#include <stdio.h>

void free_tree(struct tree *tree)
{
    if (!tree)
        return;
    else if (tree->token == TOKEN_COMPOUND_LIST_BREAK)
    {
        free_tree(((struct node_token_compound_list_break *)tree->node)->and_or);
    }
    else if (tree->token == TOKEN_ELEMENT)
    {
        if (((struct node_token_element *)tree->node)->redirection)
            free_tree(((struct node_token_element *)tree->node)->redirection);
        if (((struct node_token_element *)tree->node)->echo)
            free_tree(((struct node_token_element *)tree->node)->echo);
        free(((struct node_token_element *)tree->node)->word);
        
    }
    else if (tree->token == TOKEN_DO_GROUP)
    {
        free_tree(((struct node_token_do_group *)tree->node)->compound_list_break);
    }
    else if (tree->token == TOKEN_RULE_FOR)
        free_tree(((struct node_token_rule_for *)tree->node)->do_group);
    else if (tree->token == TOKEN_DO_GROUP)
        free_tree(((struct node_token_do_group *)tree->node)->compound_list_break);
    else if (tree->token == TOKEN_RULE_WHILE)
    {
        free_tree(((struct node_token_rule_while *)tree->node)->compound_list_break);
        free_tree(((struct node_token_rule_while *)tree->node)->do_group);
    }
    else if (tree->token == TOKEN_RULE_UNTIL)
    {
        free_tree(((struct node_token_rule_while *)tree->node)->compound_list_break);
        free_tree(((struct node_token_rule_while *)tree->node)->do_group);
    }

    else if (tree->token == TOKEN_PIPELINE)
    {
        free_tree(((struct node_token_pipeline *)tree->node)->command_one);
        if (((struct node_token_pipeline *)tree->node)->command_two)
            free_tree(((struct node_token_pipeline *)tree->node)->command_two);
    }
    else if (tree->token == TOKEN_INPUT)
    {
        free_tree(((struct node_token_input *)tree->node)->list);
    }
    else if (tree->token == TOKEN_LIST)
    {
        free_tree(((struct node_token_list *)tree->node)->and_or);
        if (((struct node_token_list *)tree->node)->next)
            free_tree(((struct node_token_list *)tree->node)->next);
    }
    else if (tree->token == TOKEN_AND_OR)
    {
        free_tree(((struct node_token_and_or *)tree->node)->pipeline_one);
        if (((struct node_token_list *)tree->node)->and_or)
            free_tree(((struct node_token_and_or *)tree->node)->pipeline_two);
    }
    else if (tree->token == TOKEN_SIMPLE_COMMAND)
    {
        if (((struct node_token_simple_command *)tree->node)->prefix)
            free_tree(((struct node_token_simple_command *)tree->node)->prefix);
        if (((struct node_token_simple_command *)tree->node)->element)
            free_tree(((struct node_token_simple_command *)tree->node)->element);
        if (((struct node_token_simple_command *)tree->node)->element_bis)
            free_tree(((struct node_token_simple_command *)tree->node)->element_bis);
    }
    else if (tree->token == TOKEN_RULE_IF)
    {
        free_tree(((struct node_token_rule_if *)tree->node)->compound_list_break_one);
        free_tree(((struct node_token_rule_if *)tree->node)->compound_list_break_two);
        if (((struct node_token_rule_if *)tree->node)->else_clause)
            free_tree(((struct node_token_rule_if *)tree->node)->else_clause);
    }
    else if (tree->token == TOKEN_ECHO)
    {
        free (((struct node_token_echo *)tree->node)->word);
    }
    else if (tree->token == TOKEN_COMMAND)
    {
        if (((struct node_token_command *)tree->node)->simple_command)
            free_tree(((struct node_token_command *)tree->node)->simple_command);
        if (((struct node_token_command *)tree->node)->shell_command)
            free_tree(((struct node_token_command *)tree->node)->shell_command);
        if (((struct node_token_command *)tree->node)->funcdec)
            free_tree(((struct node_token_command *)tree->node)->funcdec);
    }
    else if (tree->token == TOKEN_SHELL_COMMAND)
    {
        if (((struct node_token_shell_command *)tree->node)->compound_list_break)
            free_tree(((struct node_token_shell_command *)tree->node)->compound_list_break);
        if (((struct node_token_shell_command *)tree->node)->compound_list)
            free_tree(((struct node_token_shell_command *)tree->node)->compound_list);
        if (((struct node_token_shell_command *)tree->node)->rule_for)
            free_tree(((struct node_token_shell_command *)tree->node)->rule_for);
        if (((struct node_token_shell_command *)tree->node)->rule_while)
            free_tree(((struct node_token_shell_command *)tree->node)->rule_while);
        if (((struct node_token_shell_command *)tree->node)->rule_until)
            free_tree(((struct node_token_shell_command *)tree->node)->rule_until);
        if (((struct node_token_shell_command *)tree->node)->rule_case)
            free_tree(((struct node_token_shell_command *)tree->node)->rule_case);
        if (((struct node_token_shell_command *)tree->node)->rule_if)
            free_tree(((struct node_token_shell_command *)tree->node)->rule_if);
    }
    free(tree->node);
    free(tree);
}

struct tree *init_tree(void)
{
    struct tree *tree = malloc(sizeof(struct tree));
    tree->left = NULL;
    tree->right = NULL;
    return tree;
}

int shellom(struct list *token_list)
{
    return tree_token_input(&token_list);
}
/*
   void tree_print_aux(FILE *fd, struct tree *tree)
   {
   if (!tree)
   return;
   if (tree->token == TOKEN_WORD)
   fprintf(fd, "%s", ((struct node_token_word *)tree->node)->word);
   if (tree->token == TOKEN_RULE_IF)
   {
   fprintf(fd, "if ");
   fprintf(fd, "%s", ((struct node_token_rule_if *)tree->node)->condition);
   fprintf(fd, "then ");
   fprintf(fd, "%s", ((struct node_token_rule_if *)tree->node)->then);
   }
   if (tree->left)
   fprintf(fd, "->");
   tree_print_aux(fd, tree->left);
   if (tree->right)
   fprintf(fd, "\n\t");
   tree_print_aux(fd, tree->right);

   }*/
void tree_print(struct tree *tree)
{
    if (!tree)
        return;
    FILE *fd = fopen("ashellom_getit_hlm.dot", "w");
    if (fd == NULL)
        return;
    fprintf(fd, "shellom graph \n{\n\t");
    //tree_print_aux(fd, tree);
    fprintf(fd, "\n}");
    fclose(fd);
}

