#include "../token/token.h"

#ifndef TREE_H
#define TREE_H

int shellom(struct list *list);
//TODO
// funcdec way to complicated
struct node_token_rule_for
{
    char *word_one;
    char *word_two;
    struct tree *do_group;
};

struct node_token_exit
{
    int value;
};

struct node_token_continue
{
    int val; //just for decoration
};
struct node_token_break
{
    int val; //just for decoration
};

struct node_token_do_group
{
    struct tree *compound_list_break;
};
struct node_token_rule_while
{
    struct tree *compound_list_break;
    struct tree *do_group;
};

struct node_token_rule_until
{
    struct tree *compound_list_break;
    struct tree *do_group;
};

struct node_token_case_item
{
    char *word;
    struct tree *compound_list_break;
};

struct node_token_case_clause
{
    struct tree *case_item;
};
struct node_token_rule_case
{
    char *word;
    struct tree *case_clause;
};


struct node_token_else_clause
{
    int which; // 1 else, 2 elif
    struct tree *compound_list_break_one;
    struct tree *compound_list_break_two;
    struct tree *else_clause;
};

struct node_token_prefix
{
    struct tree *node_assignment_word;
    struct tree *redirection;
};

struct node_token_redirection
{
    int which; // cf pdf
    char *ionumber;
    char *word;
    char *heredoc;
};
struct node_token_shell_command
{
    int which; // 1 {}, 2(), 3 rule_for, 4 rule_while, 5 rule_until, 6 rule_case, 7 rule_if
    struct tree *compound_list_break;
    struct tree *compound_list;
    struct tree *rule_for;
    struct tree *rule_while;
    struct tree *rule_until;
    struct tree *rule_case;
    struct tree *rule_if;
};
struct node_token_simple_command
{
    int which; // 1 prefix, 2 prefix and element
    struct tree *prefix;
    struct tree *element;
    struct tree *element_bis;
};


struct node_token_command
{
    //TODO
    int which; // 1 simple_command, 2 shell_command, 3 funcdec
    struct tree *simple_command;
    struct tree *shell_command;
    struct tree *funcdec;

    struct tree *redirection;
};

struct node_token_pipeline
{
    struct tree *command_one;
    struct tree *command_two;
};
struct node_token_and_or
{
    struct tree *pipeline_one;
    int and_or; // 0 == nothing 1 == and 2 == or
    struct tree *pipeline_two;
};
struct node_token_compound_list_break
{
    struct tree *and_or;
    struct tree *next;
};
struct node_token_rule_if
{
    //TODO else coming after
    struct tree *compound_list_break_one;
    struct tree *compound_list_break_two;
    struct tree *else_clause;
};

struct node_token_element
{
    int which; //1 word 2 redirection
    char *word;
    struct tree *redirection;
    struct tree *echo;
    struct tree *continu;
    struct tree *brea;
};

struct node_token_echo
{
    char *word;
};

struct node_token_list
{
    struct tree *and_or;
    struct tree *next;
};
struct node_token_input
{
    struct tree *list;
};
struct tree
{
    enum token token;
    void *node;
    struct tree *left;
    struct tree *right;
};

void free_tree(struct tree *tree);
struct tree *init_tree(void);
struct tree *treeify(struct list *token_list);
struct tree *tree_token_else_clause(struct list **token_list);
void print_tree(struct tree *tree);

struct tree *tree_token_compound_list_break(struct list **token_list);
struct tree *tree_token_element(struct list **token_list);
#endif
