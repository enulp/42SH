#ifndef READ_INPUT_H
#define READ_INPUT_H

#include "../token/token.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <readline/readline.h>
#include <readline/history.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <signal.h>
#include <err.h>

/**
 *    \brief Reads input from user
 *    \details Reads input from stdin while displaying a prompt (42sh)
 *    on the shell
 */
int read_input(struct shell *shell);

/**
 *    \brief Reads file content
 * Tries to read the file from its path if the path is non valid, the program
 * will exit with a NULL error code
 *    \param path to the file
 */
int read_file(struct shell *shell, char *path);
int is_interactive (void);
char *get_next_line (const char *prompt);

#endif /* ! READ_INPUT */
