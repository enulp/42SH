#include "read_input.h"
#include "../token/token.h"
#include "../tree/tree.h"
#include "../exec/exec.h"

int is_interactive (void)
{
    int tty = rl_instream ? fileno (rl_instream) : fileno (stdin);

    return isatty (tty);
}

static void prep_terminal (int meta_flag)
{
    if (is_interactive ())
        rl_prep_terminal (meta_flag);
}

char *get_next_line (const char *prompt)
{
    rl_prep_term_function = prep_terminal;

    if (!is_interactive ())
        prompt = NULL;

    return readline (prompt);
}


void sigintHandler (int sig_num)
{
    sig_num = sig_num;
    signal(SIGINT, sigintHandler);
}

void read_interactive (struct shell *shell)
{
    char *buffer = malloc(sizeof(char *));
    while (buffer)
    {
        signal(SIGINT, sigintHandler);
        buffer = get_next_line ("42sh$ ");
        if (buffer == NULL)
            exit(1);
        if (*buffer == '\0')
            continue;
        struct list *list = tokenize_string(buffer);
        shellom(list);
        if (shell->print_tree)
        //    tree_print(tree);
        free(list);
        //print_tree(tree);
    }
    free(buffer);
}

int read_non_int (struct shell *shell)
{
    char *buffer = calloc(256, sizeof(char));
    char *input = calloc(2048, sizeof(char));
    if (!buffer || !input)
    {
        free(buffer);
        free(input);
        err(12, "calloc error");
    }
    while (!feof(stdin))
    {
        fread(buffer, 2048, sizeof(char), stdin);
        strncat(input, buffer, 256);
        memset(buffer, 0, 256);
    }
    if (*input == '\0')
        return 0;
    struct list *list = tokenize_string(input);
    //print_token(list);
    int ret = shellom(list);
    if (shell->print_tree)
    {}
    //    tree_print(tree);
    free(list);
    free(buffer);
    free(input);
    return ret;
}

int read_input (struct shell *shell)
{
    if (isatty (STDIN_FILENO))
        read_interactive(shell);
    else
        return read_non_int(shell);
    return 0;
}

int is_exec (char *path)
{
    struct stat st;
    if (stat (path, &st) == -1)
        err (14, path);
    if (st.st_mode & S_IXUSR)
        return 1;
    return 0;
}

int read_file (struct shell *shell, char *path)
{
    FILE *fd = fopen (path, "r");
    size_t size = 0;
    if (!fd)
        err (127, path);
    if (!is_exec (path))
        exit (126);
    fseek (fd, 0, SEEK_END);
    size = ftell (fd);
    fseek (fd, 0, SEEK_SET);
    char *buffer = malloc (size + 1);
    fread (buffer, 1, size, fd);
    fclose (fd);
    buffer[size] = '\0';
    struct list *list = tokenize_string(buffer);
    int ret = shellom(list);
    if (shell->print_tree)
    {}//tree_print(tree);
    free(list);
    free (buffer);
    return ret;
}
