#!/bin/sh

[ ! -d "build" ] && mkdir build

cd build
cmake ..
make
cp 42sh ..
cp 42sh ../tests
