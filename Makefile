CC=gcc
HEADER= src/conditions/conditions.h \
	src/token/token.h \
	src/tree/tree.h \
	src/shell/shell.h \
	src/exec/exec.h \
	src/input/read_input.h
FLAGS=  -D_GNU_SOURCE -Isrc/conditions -Isrc/exec -lreadline -Isrc/input -Isrc/token -Isrc/shell -Isrc/tree -g -Werror -Wextra -Wall -pedantic -std=c99
OBJS= src/conditions/conditions.o \
	src/token/token.o \
	src/tree/tree.o \
	src/input/read_input.o \
	src/shell/shell.o \
	src/main.o
OBJSBIS= conditions.o \
	token.o \
	tree.o \
	read_input.o \
	exec.o \
	shell.o \
	main.o
all:clean ${OBJSBIS} exec
	${CC} ${FLAGS} -o 42sh *.o  ${LDFLAGS}

exec:
	$(CC) $(FLAGS) -c src/exec/* $(LDFLAGS)
main.o: 
	$(CC) $(FLAGS) -c -o $@ src/main.c ${LDFLAGS}
conditions.o:
	$(CC) $(FLAGS) -c -o $@ src/conditions/conditions.c ${LDFLAGS}
shell.o:
	$(CC) $(FLAGS) -c -o $@ src/shell/shell.c ${LDFLAGS}
exec.o:
	$(CC) $(FLAGS) -c -o $@ src/exec/exec.c ${LDFLAGS}
read_input.o:
	$(CC) $(FLAGS) -c -o $@ src/input/read_input.c ${LDFLAGS}
token.o:
	$(CC) $(FLAGS) -c -o $@ src/token/token.c ${LDFLAGS}
tree.o:
	$(CC) $(FLAGS) -c -o $@ src/tree/tree.c ${LDFLAGS}

check:
	cd tests
	./tests/testsuite.py 42sh

clean:
	${RM} ${OBJS} ${OBJSBIS} *.o 42sh
